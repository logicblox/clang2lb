package generator;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;

/**
 *
 * @author kostasferles
 */
public class Options {
    
    private final static FileSystem fSystem = FileSystems.getDefault();
    
    private final static Options instance = new Options();
    
    private Options(){}
    
    public static Options getInstance(){
        return instance;
    }
    
    @Argument(required = true, 
              metaVar = "<spec-file>",
              usage = "the ast specification input file")
    private String specInputFile;
    
    @Option(name = "-af",
            aliases = {"--aux-file"},
            usage = "the auxiliary file for the specification")
    private String auxInputFile;
    
    @Option(name = "-ps",
            aliases = {"--print-schema"},
            usage = "prints the LogicQL schema")
    private boolean printSchema = false;
    
    @Option(name = "-cp",
            aliases = {"--create-plugin"},
            usage = "creates the Clang plugin",
            depends = {"-af", "-csvOut"})
    private boolean createPlugin = false;
    
    @Option(name = "-ic",
            aliases = {"--import-code"},
            usage = "generates the LogicQL code that imports the csv files")
    private boolean  genImportCode = false;
    
    @Option(name = "-d",
            aliases = {"--delim"},
            usage = "the delimiter for the csv files (default value = \",\")",
            depends = {"-ic"})
    private String delimeter = ",";
    
    @Option(name = "-h",
            aliases = {"--help"},
            usage = "prints usage information")
    private boolean help = false;
    
    @Option(name = "-csvOut",
            aliases = {"--csv-output-dir"},
            depends = {"-cp"},
            usage = "the output directory for the csv files",
            handler = PathOptionHandler.class)
    private Path csvOutDir;
    
    public String getSpecInputFile(){
        return this.specInputFile;
    }
    
    public String getAuxInputFile(){
        return this.auxInputFile;
    }
    
    public boolean printSchema(){
        return this.printSchema;
    }
    
    public boolean createPlugin(){
        return this.createPlugin;
    }
    
    public boolean genImportCode(){
        return this.genImportCode;
    }
    
    public String getDelim(){
        return this.delimeter;
    }
    
    public Path getCsvOutDir(){
        return this.csvOutDir;
    }
    
    public boolean getHelp(){
        return this.help;
    }
    
    public static class PathOptionHandler extends OptionHandler<Path> {
        
        public PathOptionHandler(CmdLineParser parser, OptionDef option, Setter<? super Path> setter){
            super(parser, option, setter);
        }

        @Override
        public int parseArguments(Parameters prmtrs) throws CmdLineException {
            try {
                Path path = fSystem.getPath(prmtrs.getParameter(0));
                
                setter.addValue(path);
            } catch (InvalidPathException exc) {
                throw new CmdLineException(owner, exc);
            }
            return 1;
        }

        @Override
        public String getDefaultMetaVariable() {
            return "<path>";
        }
    }

}
