
#include <map>
#include <list>
#include <vector>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/fstream.hpp>

class CsvWriter {

  typedef boost::filesystem::ofstream ofstream;
  typedef std::string string;
  typedef std::map<string,ofstream*> fileMap;
  typedef std::vector<string> vector;

  fileMap csvFiles;

  const string delim;

  const string csvDir;

  ofstream& getFile(string relation){
    fileMap::iterator it = csvFiles.find(relation);
    if( it == csvFiles.end() ){
      ofstream *newFile = new ofstream(string(csvDir + "/" + relation + ".csv").c_str(), ofstream::out | ofstream::app);
      csvFiles[relation] = newFile;
      return *newFile;
    }
    else{
      return *(it->second);
    }
  }

 public:

 CsvWriter(const string delim, const string csvDir)
   : delim(delim), csvDir(csvDir)
  {}


  void writeLine(std::string relation, std::vector<std::string>* elements){
    vector::const_iterator end = elements->end();
    vector::const_iterator it = elements->begin();

    ofstream& out = getFile(relation);

    out << *it;
    it++;
    for( ; it != end ; it++)
      out << delim << *it;
      out << std::endl;
  }

  const string getDelim() const{
    return delim;
  }

  const string getOutDir() const{
    return csvDir;
  }

  ~CsvWriter(){
    fileMap::iterator end = csvFiles.end();
    for(fileMap::iterator it = csvFiles.begin() ; it != end ; it++)
      delete it->second;
  }
};
