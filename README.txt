1. Install Clang-llvm:
   -Follow the instructions given here (http://clang.llvm.org/docs/LibASTMatchersTutorial.html#step-0-obtaining-clang)
     *Important: Do NOT skip the last step that sets the compiler of clang to be clang itself.

2. Build clang-astspec and clang2lb projects:
   2.1 Download install bash script from here (https://www.dropbox.com/s/3k4j77k20y8xrei/install.sh)
    and simply run ./install.sh (this will download both projects and all the dependenies).
    The clang2lb.jar (located inside the build directory) is the program that generates the LogicQL schema,
    the LogicQL import code and the Clang plugin that generates the csv files.
    Run: java -jar clang2lb.jar -h for the options.
    Run: java -jar clang2lb.jar pathTo/clang_ast.spc -af pathTo/clang_ast.aux -ps -ic -cp -csvOut [path that contains the csv files]
      Note: clang_ast.* files are in the clang-astspec directory
    The above command generates 3 files:
    ClangAst.logic -- The LogicQL schema
    ClangAst-import.logic -- The LogicQL import code
    ClangAst.h -- A part of the the clang plugin that generates the csv files
   2.2 The folder csvGen contains all the files that a clang plugins needs for compilation.
    After running the command in step 2.1 copy ClangASt.h inside the csvGen folder and then copy the whole directory inside the
    clang plugins directory (located inside pathToLLVMsrc/llvm/tools/clang/examples/).
    In the same directory edit the CMakeLists.txt and append the following line:
      add_subdirectory(csvGen)
    Finally, go to the clang-llvm build directory and run "ninja CsvGenerator" (you should wait some seconds the first time you run this).
   2.3 To run the plugin on some code read the README file inside the csvGen directory and make sure that the output directory is
   the same as in step 2.1.
