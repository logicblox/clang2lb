set -ex
CLASSPATH=/usr/share/java/antlr4-runtime-4.5.1.jar:/usr/share/java/args4j.jar:/usr/share/java/jgrapht0.8.jar/:../clang-astspec/lib/clang-astspec.jar

mkdir -p classes
javac -cp $CLASSPATH -d classes `find . -name '*.java'`

cd classes
echo "Class-Path: antlr4-runtime-4.5.1.jar args4j.jar jgrapht0.8.jar clang-astspec.jar" > Manifest
jar cfme clang2lb.jar Manifest generator.Driver `find -name '*.class'`

mkdir -p ../lib
mv clang2lb.jar ../lib

ln -sf /usr/share/java/antlr4-runtime-4.5.1.jar ../lib/antlr4-runtime-4.5.1.jar
ln -sf /usr/share/java/args4j.jar ../lib/args4j.jar
ln -sf /usr/share/java/jgrapht0.8.jar ../lib/jgrapht0.8.jar
ln -sf ../../clang-astspec/lib/clang-astspec.jar ../lib/clang-astspec.jar

mkdir -p ../bin
cat > ../bin/clang2lb <<EOF
#! /bin/bash
exec java -jar lib/clang2lb.jar "\$@"
EOF

chmod +x ../bin/clang2lb
