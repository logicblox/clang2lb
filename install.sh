set -e

mkdir -p include
mkdir -p share/lql
mkdir -p share/csv-generator-base

bin/clang2lb clang_ast.spc -af clang_ast.aux -ps -ic -cp -csvOut share/csv-generator-base

mv *.logic share/lql
sed -i "s|share/csv-generator-base/|./|" share/lql/*    # */
mv ClangAst.h include
