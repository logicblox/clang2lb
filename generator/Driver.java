package generator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import printer.PrintClangCsvPlugin;
import printer.PrintLogicQLSchema;
import printer.PrintLogicQLImportCode;
import specwalker.ASTSpecWalker;
import symboltable.SymbolTable;

/**
 *
 * @author kostasferles
 */
public class Driver {
    
    private static void usage(CmdLineParser clp){
        System.err.print("java -jar <clang2lb>");
        clp.printSingleLineUsage(System.err);
        System.err.println("\n");
        clp.printUsage(System.err);
        System.exit(1);
    }
    
    public static void main(String[] args){
        Options bean = Options.getInstance();
        
        CmdLineParser clp = new CmdLineParser(bean);
        SymbolTable table = SymbolTable.getInstance();
        
        try{
            clp.parseArgument(args);
        }
        catch(CmdLineException ex){
            if(!bean.getHelp()){
                System.err.println("Error: " + ex.getMessage() + "\nUsage:\n");
                usage(clp);
            }
        }
        
        if(bean.getHelp())
            usage(clp);
        FileSystem fSystem = FileSystems.getDefault();
        boolean specParsed = false;
        ASTSpecWalker walker = new ASTSpecWalker();
        if(bean.printSchema()){
            String inputFile = bean.getSpecInputFile();
            
            try {
                if(clangAST.Driver.parseSpecFile(inputFile) != 0)
                        System.exit(1);
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
                System.exit(1);
            }
            
            String outputFile = table.getClangSpecName() + ".logic";
            Path outputPath = fSystem.getPath(outputFile);
            
            try (BufferedWriter writer = Files.newBufferedWriter(outputPath, StandardCharsets.UTF_8);){
                
                walker.walk(new PrintLogicQLSchema(writer));
                specParsed = true;
            }
            catch(IOException ex){
                System.err.println(ex.getMessage());
                System.exit(1);
            }
        }
        
        if(bean.createPlugin()){
            try {
                if(!specParsed){
                    if(clangAST.Driver.parseSpecAndAuxFile(bean.getSpecInputFile(), bean.getAuxInputFile()) != 0)
                        System.exit(1);
                }
                else {
                    if(clangAST.Driver.parseAuxFile(bean.getAuxInputFile()) != 0)
                        System.exit(1);
                }
                
                String outputFilename = table.getClangSpecName() + ".h";
                Path outputFile = fSystem.getPath(outputFilename);
                
                try(BufferedWriter writer = Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8);){
                    walker.walk(new PrintClangCsvPlugin(writer));
                }

            } catch (IOException ex) {
                System.err.println(ex.getMessage());
                System.exit(1);
            }
        }
        
        if(bean.genImportCode()){
            try{
                if(!specParsed){
                    if(clangAST.Driver.parseSpecFile(bean.getSpecInputFile()) != 0)
                        System.exit(1);
                }
                
                String outputFilename = table.getClangSpecName() + "-import.logic";
                Path outputFile = fSystem.getPath(outputFilename);
                
                try(BufferedWriter writer = Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8);){
                    Path csvOutDir = bean.getCsvOutDir();
                    if(Files.exists(csvOutDir)){
                        if(!Files.isDirectory(csvOutDir)){
                            System.err.println(csvOutDir.toString() + "is not a directory");
                            System.exit(1);
                        }
                    }
                    else{
                        Files.createDirectories(csvOutDir);
                    }
                    walker.walk(new PrintLogicQLImportCode(writer, csvOutDir, bean.getDelim()));
                }
            }
            catch (IOException ex){
                System.err.println(ex.getMessage());
                System.exit(1);
            }
        }
        System.exit(0);
    }
    
}
