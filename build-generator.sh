set -x
CLANG=/usr/lib/llvm-3.8/lib
LLVM_INCLUDE=/usr/lib/llvm-3.8/include
CLANG_INCLUDE=/usr/lib/llvm-3.8/include

clang++ -g -std=c++11 -D__STDC_LIMIT_MACROS -D__STDC_CONSTANT_MACROS -fPIC -I/usr/include/boost -I$CLANG_INCLUDE -I$LLVM_INCLUDE -Iinclude -IcsvGen -o CsvGenerator.cpp.o -c csvGen/CsvGenerator.cpp
clang++ -g -fPIC -shared -Wl,-soname,CsvGenerator.so -o CsvGenerator.so CsvGenerator.cpp.o -L$CLANG/lib -lLLVMMC -lLLVMObject -lLLVMSupport -ldl -lpthread -L$CLANG/lib -lclangAST -lclangLex -lclangBasic -lboost_system 

mkdir -p lib
mv CsvGenerator.so lib
