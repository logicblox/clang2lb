package util;

import java.io.BufferedWriter;
import java.io.IOException;
import symboltable.Property;

/**
 *
 * @author kostasferles
 */
public class PrintUtil {
    
    public static String getFullPropertyName(Property pr){
        StringBuilder rv = new StringBuilder();
        
        return rv.append(pr.getParent().getName()).append(":").append(pr.getName()).toString();        
    }
    
    public static void writeToFile(BufferedWriter out, String s){
        try{
            out.write(s);
        }
        catch(IOException ex){
            /*
             * TODO: log
             */
            System.err.println(ex.getMessage());
            System.exit(1);
        }
    }
    
    public static String quote(String s){
        return "\"" + s + "\"";
    }
    
}
