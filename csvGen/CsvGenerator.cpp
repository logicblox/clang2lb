
#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/CompilerInstance.h"
#include "llvm/Support/raw_ostream.h"
#include "ClangAst.h"

using namespace std;
using namespace clang;

namespace {

  bool ClangAst::TraverseDecl(Decl* D){
    if(D != NULL){
      integer DeclId = getUID(D).val;
      if(exploredDecls.find(DeclId) == exploredDecls.end()){
        bool rv = RecursiveASTVisitor<ClangAst>::TraverseDecl(D);
	if(rv){
	  exploredDecls.insert(DeclId);
	  (*exploredDeclsFile) << DeclId << endl;
	}
	else{
	  return false;
	}
      }
    }
    return true;
  }

  bool ClangAst::TraverseStmt(Stmt* S){
    if(S != NULL){
      integer StmtId = getUID(S).val;
      if(exploredStmts.find(StmtId) == exploredStmts.end()){
        bool rv = RecursiveASTVisitor<ClangAst>::TraverseStmt(S);
	if(rv){
	  exploredStmts.insert(StmtId);
	  (*exploredStmtsFile) << StmtId << endl;
	}
	else{
	  return false;
	}
      }
    }
    return true;
  }

  bool ClangAst::TraverseType(QualType T){
    if(!T.isNull()){
      integer TypeId = getUID(T).val;
      if(exploredTypes.find(TypeId) == exploredTypes.end()){
        bool rv = RecursiveASTVisitor<ClangAst>::TraverseType(T);
	if(rv){
	  exploredTypes.insert(TypeId);
	  (*exploredTypesFile) << TypeId << endl;
	}
	else{
	  return false;
	}
      }
    }
    return true;
  }

  class CsvGeneratorConsumer : public ASTConsumer {
  public:
  
    CsvGeneratorConsumer(const ASTContext *Context, const string delim, const string csvOutDir)
      :Visitor(Context, new CsvWriter(delim, csvOutDir))
    {

    }

    virtual void HandleTranslationUnit(ASTContext& Context) {
      Visitor.RecursiveASTVisitor<ClangAst>::TraverseDecl(Context.getTranslationUnitDecl());
    }

    ClangAst Visitor;
  };

  class CsvGeneratorAction : public PluginASTAction {
  protected:
    FileID FId;
    SourceManager* SManager;

    std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI, llvm::StringRef file) {
      return std::unique_ptr<ASTConsumer>(new CsvGeneratorConsumer(&(CI.getASTContext()), string(delim), string(csvOutDir)));
    }

    std::string csvOutDir;

    std::string delim;

    bool ParseArgs(const CompilerInstance &CI,
		   const std::vector<std::string>& args) {
      bool outDirFound = false;
      delim = std::string(",");

      for (unsigned i = 0, e = args.size(); i != e; ++i) {
	std::string arg = args[i];
	if(arg == std::string("-outDir")){
	
	  if(++i < e){
	    csvOutDir = args[i];
	    outDirFound = true;
	    continue;
	  }
	}

	if(arg == "-delim"){
	  delim = arg;
	  continue;
	}
	llvm::errs() << "Invalid option: " << arg << "\n";
	PrintHelp(llvm::errs());
	return false;
      }

      if(!outDirFound){
	llvm::errs() << "Output directory for csv files not found\n";
	PrintHelp(llvm::errs());
	return false;
      }

      return true;
    }

    void PrintHelp(llvm::raw_ostream& ros) {
      ros << "Options:\n-outDir\toutput csv directory (required)\n-delim\tdelimeter for the csv file (defaul=\",\")\n";
    }

  };

}

static FrontendPluginRegistry::Add<CsvGeneratorAction>
X("csv-gen", "clang AST to csv files");
