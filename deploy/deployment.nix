{
  network.description = "clang-astspec default deployment";

  resources.ec2KeyPairs.clang-astspec-keypair = {
    region = "us-east-1";
    accessKeyId = "LB_Dev";
  };

  machine = { pkgs, resources, ... }: {
    imports = [
      <lbdevops/nixos/query-tool.nix>
      <lbdevops/nixos/installer.nix>
      <lbdevops/nixos/nginx.nix>
      <lbdevops/nixos/base.nix>
    ];

    services.logicblox = {
      enable = true;

      logicblox = builtins.storePath <logicblox>;

      bloxweb = builtins.storePath <bloxweb>;

      newRuntime = true;
    };
    logicblox.application.installer = builtins.storePath <installer>;
    logicblox.application.installFlags = builtins.storePath <clang-ast-files>;

    environment.systemPackages = [ (pkgs.writeScriptBin "add-asts" ''
      #! ${pkgs.stdenv.shell}
      function cleanup {
        rm -fR $TMP
      }
      trap cleanup EXIT
      TMP=`mktemp -d`
      cd $TMP
      tar xv
      lb exec asts -f ${builtins.storePath <clang-ast-files>}/share/lql/ClangAst-import.logic --cwd
    '') ];
    

    deployment.targetEnv = "ec2";
    deployment.ec2.region = "us-east-1";
    deployment.ec2.instanceType = "m1.large";
    deployment.ec2.accessKeyId = "LB_Dev";
    deployment.ec2.securityGroups = [ "admin" "http-https-world" ];
    deployment.ec2.keyPair = resources.ec2KeyPairs.clang-astspec-keypair.name;
  };
}
