{ clang2lb-src ? ./.
, clang-astspec-src ? ../clang-astspec
, src_logicblox
}:

let
  pkgs = import <nixpkgs> {};

  stdenv' = pkgs.llvmPackages_38.stdenv;

  inherit (pkgs) fetchurl stdenv jdk lib runCommand boost fetchgit stdenvAdapters
    perl zlib openssl flex ncurses bc gperftools python pythonPackages procps less;

  unwrapped_clang =
    pkgs.llvmPackages_38.clang.cc;

  clang =
    pkgs.clang_38.cc;

  llvm =
    pkgs.llvmPackages_38.llvm;
 
  antlr = fetchurl {
    url = http://www.antlr.org/download/antlr-4.1-complete.jar;
    sha256 = "17bggcqq9ia39a0n178v8nw2qyccdapsr6w9idbibym3r4s6j45m";
  };

  args4j = fetchurl {
    url = http://repo1.maven.org/maven2/args4j/args4j/2.0.25/args4j-2.0.25.jar;
    sha256 = "19jb7k4px8j34wcvlsvdhls0pai6vps2ixac4js9b9nsixb1fvhh";
  };

  jgrapht = stdenv.mkDerivation {
    name = "jgrapht-jdk1.6.jar";

    src = fetchurl {
      url = mirror://sourceforge/jgrapht/jgrapht-0.8.3.tar.gz;
      sha256 = "0idalyrdk6zhyhazzxwpjsx6hank8xss33cjjixknxr0h98350my";
    };

    phases = [ "unpackPhase" "installPhase" ];

    installPhase = "mv jgrapht-jdk1.6.jar $out";
  };

  classPath = [ antlr args4j jgrapht ];

  astClassPath = classPath ++ [ "${builds.clang-astspec}/lib/clang-astspec.jar" ];

  config = "release";
  deps = import (src_logicblox + "/nix/lb-deps.nix") { inherit pkgs config; };

  builds = {
    clang-astspec = stdenv.mkDerivation {
      name = "clang-astspec";

      buildInputs = [ jdk ];

      buildCommand = ''
        mkdir classes
        javac -cp ${lib.concatStringsSep ":" classPath} -d classes `find ${clang-astspec-src} -name '*.java'`
        cd classes
        echo "Class-Path: ${builtins.toString (map baseNameOf classPath)}" > Manifest
        jar cfme clang-astspec.jar Manifest clangAST.Driver `find -name '*.class'`

        mkdir -p $out/lib
        mv clang-astspec.jar $out/lib
        ${lib.concatStringsSep "\n" (map (x: "ln -s ${x} $out/lib/${baseNameOf x}") classPath)}
      '';
    };

    clang2lb = stdenv.mkDerivation {
      name = "clang2lb";

      buildInputs = [ jdk ];

      buildCommand = ''
        mkdir classes
        javac -cp ${lib.concatStringsSep ":" astClassPath} -d classes `find ${clang2lb-src} -name '*.java'`
        cd classes
        echo "Class-Path: ${builtins.toString (map baseNameOf astClassPath)}" > Manifest
        jar cfme clang2lb.jar Manifest generator.Driver `find . -name '*.class'`

        mkdir -p $out/lib
        mv clang2lb.jar $out/lib
        ${lib.concatStringsSep "\n" (map (x: "ln -s ${x} $out/lib/${baseNameOf x}") astClassPath)}
        mkdir -p $out/bin
        cat > $out/bin/clang2lb <<EOF
        #! ${stdenv.shell}
        exec ${jdk}/bin/java -jar $out/lib/clang2lb.jar "\$@"
        EOF
        chmod +x $out/bin/clang2lb
      '';
    };

    clang-ast-files = stdenv.mkDerivation {
      name = "clang-ast-files";

      buildInputs = [ builds.clang2lb ];

      buildCommand = ''
        mkdir -p $out/include
        mkdir -p $out/share/lql
        mkdir -p $out/share/csv-generator-base

        clang2lb ${clang-astspec-src}/clang_ast.spc -af ${clang-astspec-src}/clang_ast.aux -ps -ic -cp -csvOut $out/share/csv-generator-base

        mv *.logic $out/share/lql
        sed -i "s|$out/share/csv-generator-base/|./|" $out/share/lql/*    # */
        mv ClangAst.h $out/include
      '';
    };

    csv-generator = stdenv'.mkDerivation {
      name = "csv-generator";

      src = "${clang2lb-src}/csvGen";

      hardeningDisable = [ "all" ];
      postUnpack = "cp ${builds.clang-ast-files}/include/ClangAst.h $sourceRoot";

      buildInputs = [ clang ];

      buildPhase = ''
        set -x


        # clang++ -DCsvGenerator_EXPORTS -DNDEBUG -D__STDC_CONSTANT_MACROS -D__STDC_FORMAT_MACROS -D__STDC_LIMIT_MACROS -D_GNU_SOURCE -DCLANG_ENABLE_ARCMT -DCLANG_ENABLE_REWRITER -DCLANG_ENABLE_STATIC_ANALYZER -I${boost.dev}/include -fPIC -fvisibility-inlines-hidden -fno-common -Woverloaded-virtual -Wcast-qual -fno-strict-aliasing -pedantic -Wno-long-long -fno-rtti -DNDEBUG -fPIC -I$sourceRoot -I${unwrapped_clang}/include -I${llvm}/include -fno-exceptions -fexceptions -O3 -o CsvGenerator.cpp.o -c CsvGenerator.cpp
        clang++ -std=c++11 -D__STDC_LIMIT_MACROS -D__STDC_CONSTANT_MACROS -fPIC -I${boost.dev}/include -I${unwrapped_clang}/include -I${llvm}/include -I$sourceRoot -IcsvGen -o CsvGenerator.cpp.o -c CsvGenerator.cpp
        clang++  -fPIC -fvisibility-inlines-hidden -fno-common -Woverloaded-virtual -Wcast-qual -fno-strict-aliasing -pedantic -Wno-long-long -fno-rtti -DNDEBUG -O3 -shared -Wl,-soname,CsvGenerator.so -o CsvGenerator.so CsvGenerator.cpp.o -L${llvm}/lib -lLLVMMC -lLLVMObject -lLLVMSupport -ldl -lpthread -L${unwrapped_clang}/lib -lclangAST -lclangLex -lclangBasic -lLLVMMC -lLLVMObject -lLLVMSupport -ldl -lpthread -L${boost}/lib -lboost_system
      '';

      installPhase = ''
        mkdir -p $out/lib
        mv CsvGenerator.so $out/lib

        ln -sv ${clang}/bin $out

        mkdir $out/nix-support
        for file in ${clang}/nix-support/*; do      # */
          ln -sv $file $out/nix-support
        done
       '';

       inherit clang;

    };

    lb-database-clang-build = stdenv'.mkDerivation {

      name = "lb-database-clang-build";
  
      inherit src_logicblox;
  
      buildInputs =
        [ perl zlib deps.cppunit openssl flex ncurses deps.boost bc
          python
          (pythonPackages.readline or null) # readline was moved into python in Nixpkgs 17.03
          pythonPackages.psutil
          gperftools deps.or_tools
          deps.icu
          builds.csv-generator
          clang
        ]
        ++ deps.jars
        ++ lib.optionals stdenv.isLinux [ procps ]
        ++ lib.optionals lib.inNixShell [ less ];
  
      propagatedBuildInputs = [ python pkgs.openjdk8 deps.protobuf ];
  
      configurePhase =
        ''
          mkdir -p $out/csvFiles
          echo "prefix = $prefix" >> Makefile.config
          unset NIX_INDENT_MAKE
          echo "CC=${clang2lb-src}/instrumented_clang/clang" >> Makefile.config
          echo "CXX=${clang2lb-src}/instrumented_clang/clang++" >> Makefile.config
        '';
        
      preBuild =
        ''
          export LB_CLANG_OUTDIR=$out/csvfiles
          mkdir -p $LB_CLANG_OUTDIR
          export LB_CLANG_CSVGEN_DIR=${builds.csv-generator}/lib
        '';
  
      makeFlags =
        [ "CONFIG=release" 
          "TCMALLOC=1"
        ];
        
      enableParallelBuilding = false;  
  
      dontInstall = true;
  
      # Don't strip debug builds so that they can be used for
      # debugging. For release builds, separate the debug info.
      dontStrip = false;
      separateDebugInfo = true;
  
      # Pass the Mercurial revision to the scripts that generate the version header files.
      BUILDREV = "000000000000";
  
      NIX_CFLAGS_COMPILE = "";
    };


  };
in builds
